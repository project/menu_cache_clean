INTRODUCTION
------------

Menu Cache Clean is a module which clean cache by menu. User need to insert
menu in given text field to clear the cache. This provides the most simplest
way to clear the cache of particular menu. No need to clear cache for 
all menu by command drush cc menu.
This module is use basically large project menu cache clear for developer side
time consuming for hook_menu callback create.

CONFIGURATION
-------------

1. Download the module from https://www.drupal.org/project/menu_cache_clean
and save it to your modules folder.
2. Ensure you have enabled and configured the Menu Cache Clean.
3. Enable the module at admin/modules.

REQUIREMENTS
------------

1. Use For Cache Clear By Menu.
2. For clear cache by menu you can insert menu in given text field and the url
of module is admin/config/development/custom-menu-rebuild/add

INSTALLATION
------------
1. https://www.drupal.org/project/menu_cache_clean
